# minimodem-win

binary minimodem for windows(both [32](/32) and [64](/64) bit)

compile minimodem for windows (build with cygwin 2020 on windows 7)  


minimodem on github: [https://github.com/kamalmostafa/minimodem](https://github.com/kamalmostafa/minimodem)

# build info
src: https://github.com/kamalmostafa/minimodem/issues/3#issuecomment-460041353
```bash 
autoreconf -i
./configure --without-alsa
make
```


# known issue  
*  play audio signal (you must save signal in file then play it.)
# examlpe usage
```bash
git clone https://gitlab.com/svrb/minimodem-win  
run cmd  
cd minimodem-win  
minimodem --tx -f out.flac -M 1200 -S 2200 1200 # send (modulation)  
minimodem --rx -f in.flac -M 1200 -S 2200 1200  # recive (demodulation)  
```
